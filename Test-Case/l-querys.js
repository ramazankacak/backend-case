'use strict';

function main() {
 
    const {BigQuery} = require('@google-cloud/bigquery');
 
    async function query() {

        const fs=require('fs');
        const events = require('events').EventEmitter;      

        const bigqueryClient = new BigQuery();
        // query 1 total user
        const sqlQuery = `SELECT count(*) , user_id
            FROM \`codewaytest.eventLog.allEvents\`
            GROUP BY user_id HAVING COUNT(*) > 1`;
        const options = {
        query: sqlQuery,
        location: 'US',
        };

        // query 2 user one event
        const sqlQuery1 = `SELECT event_time,event_name,user_id
            FROM \`codewaytest.eventLog.allEvents\`
            WHERE user_id = @corpus and event_name=@corpus1`;
        const options1 = {
        query: sqlQuery1,
        location: 'US',
        params: {corpus: '36DoicuIPn',corpus1:'search'},
        };

        // query search event duration between
        const sqlQuery2 = `SELECT user_id , MAX(event_time)-MIN(event_time)
            FROM \`codewaytest.eventLog.allEvents\`
            WHERE user_id = @corpus and event_name=@corpus1
            GROUP BY user_id HAVING COUNT(*) > 1`;
        const options2 = {
        query: sqlQuery2,
        location: 'US',
        params: {corpus: '36DoicuIPn',corpus1:'search'},
        };
           
        const [rows] = await bigqueryClient.query(options);
        const [rows1] = await bigqueryClient.query(options1);
        const [rows2] =await bigqueryClient.query(options2);

        const eventFunc = function() {
            const queryExec = new events();
            queryExec.emit('start');
            return queryExec;
        };

        const func = eventFunc();
        func.on('start', function(){
            
            console.log('1:');
            console.log(rows.length);   

            console.log('2:');
            rows1.forEach(
                row1 => console.log(row1)
            );
            
            console.log('3:');
            rows2.forEach(
                row2 => console.log(row2)
            );
        });

        func.emit('start');
       
    }
    query();
  }
 
main();

