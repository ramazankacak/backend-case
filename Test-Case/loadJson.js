'use strict';
 
function main(datasetId = 'eventLog', tableId = 'allEvents') {

  const {BigQuery} = require('@google-cloud/bigquery');
  const {Storage} = require('@google-cloud/storage');
 
  const bucketName = 'dummyeventtest';
  const filename = 'dummyEvents.json';
 
  async function loadJSONFormat() {

    const bigquery = new BigQuery();
    const storage = new Storage();
    const metadata = {
      sourceFormat: 'NEWLINE_DELIMITED_JSON',
      autodetect: true,
      location: 'US',
    };
     const [job] = await bigquery
      .dataset(datasetId)
      .table(tableId)
      .load(storage.bucket(bucketName).file(filename), metadata);
    console.log(`Job ${job.id} completed.`);
 
    const errors = job.status.errors;
    if (errors && errors.length > 0) {
      throw errors;
    }
  }
  loadJSONFormat();
}
main(...process.argv.slice(2));