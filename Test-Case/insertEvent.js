function main(datasetId = 'eventLog', tableId = 'allEvents') {

  const {BigQuery} = require('@google-cloud/bigquery');
  const bigquery = new BigQuery();

  async function insertData() {

    const rows = [
      {
      type: 'event',
      app_id:'com.codeway.testapp',
      session_id:'9FDA74C2-AB57-4840-87D0-64324772B5A2',
      event_name:'background',
      event_time: 1589623711,
      page: 'main',
      country: 'TR',
      region: 'İzmir',
      city: 'Akdeniz',
      user_id: 'Uu1qJzlfrxYxOS5z1kfAbmSA5p44'
      },   
    ];

    // Insert data into a table
    await bigquery
      .dataset(datasetId)
      .table(tableId)
      .insert(rows);
    console.log(`Inserted ${rows.length} rows`);
  }
  insertData();
}
 
main();